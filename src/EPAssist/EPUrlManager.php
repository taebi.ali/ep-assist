<?php
namespace EPAssist;
class EPUrlManager {

    private static $instance;
    private $routes;

    public static function getInstance() {
        if(self::$instance == NULL) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function addRoute($name, $path, $view){
        $this->routes[$name] = [
            'name'=>$name,
            'path'=>$path,
            'view'=>$view
        ];
    }

    private function __construct() {
        add_action( 'init', array($this,'callback_init_internal' ) );
        add_filter( 'query_vars', array($this,'callback_query_vars' ) );
        add_action( 'parse_request', array($this,'callback_request' ) );
    }

    function callback_init_internal()
    {

        foreach ($this->routes as $route) {
            add_rewrite_rule(
                $route['name'],
                plugin_dir_url(__FILE__) . $route['path'],
                'top'
            );
        }
        //SavaLog('url_manager_instance_routes', $this->routes);
        $new_routes_key = md5(json_encode($this->routes));
        $cache = EPCache::get('sv_url_manager_routes');
        if($cache){
            if($new_routes_key != $cache){
                self::activate();
            }
        }else{
            self::activate();
        }
        EPCache::set('sv_url_manager_routes', $new_routes_key, 60*60*24);

    }

    function callback_query_vars( $query_vars )
    {
        $query_vars[] = 'callback';
        return $query_vars;
    }

    function callback_request( &$wp )
    {
        //SavaLog('url_manager_instance_check', $wp->query_vars);
        foreach ($this->routes as $route) {

            if ( isset($wp->query_vars['name']) &&  $wp->query_vars['name'] == $route['name']) {

                //SavaLog('url_manager_instance_found', [$wp->query_vars, $route['view']]);
                $class =  $route['view'][0];
                $method = $route['view'][1];
                //SavaLog('url_manager_instance_view', [$class, $method]);
                if(method_exists(new $class, $method)){
                    $class::$method();
                    exit();
                }

            }
        }


        return;
    }

    static function activate() {
        global $wp_rewrite;
        flush_rewrite_rules();
        $wp_rewrite->flush_rules(true);
    }
}

$url_manager = \EPAssist\EPUrlManager::getInstance();


register_deactivation_hook(__FILE__,'flush_rewrite_rules');
register_activation_hook(__FILE__,'\EPAssist\EPUrlManager::activate');