<?php
namespace EPAssist;
class EPString
{

    public static function EN2PN($srting)
    {
        $EN = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $PN = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        return str_replace($EN, $PN, $srting);
    }
    public static function PN2EN($srting)
    {
        $EN = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $PN = array('۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹');
        return str_replace($PN, $EN, $srting);
    }
}