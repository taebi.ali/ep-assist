<?php
namespace EPAssist;
class EPJobs
{
    const PJOB_PREFIX = 'shc_jobs__';
    /**
     * @var array $jobs
     */
    public $jobs;
    public function __construct($jobs)
    {
        $this->jobs = $jobs;
        //add_action( 'init', [$this, 'schedule_sv_cron']);
    }

    public function schedule_sv_cron(){
        foreach ($this->jobs as $job) {
            $title = self::PJOB_PREFIX.$job;
            if ( ! wp_next_scheduled ( $title) ) {
                #PTelegram::send(PTelegram::DEFAULT_CHAT, 'schadule');
                $result = wp_schedule_event( time(), $job, $title);
                EPTelegram::send(EPTelegram::get_debug_chat(), $title.'_'.json_encode($result));
            }
        }
    }
    public static function Add($period, $function){
        $title = self::PJOB_PREFIX.$period;
        add_action( $title, $function);
    }

}