<?php


namespace EPAssist;


use stdClass;

class EPCache
{
    const CACHE_KEY = 'ep';
    public static function get($key){
        $cache_file_path = plugin_dir_path(__DIR__.'data').'/.'.self::CACHE_KEY.'.cache';
        if(file_exists($cache_file_path)){
            $data =  file_get_contents($cache_file_path);
            $object = json_decode($data);

            if(isset($object->$key)){
                return $object->$key;
            }
        }
        return null;
    }

    public static function set($key, $value, $ttl){
        $cache_file_path = plugin_dir_path(__DIR__.'data').'/.'.self::CACHE_KEY.'.cache';
        $object = new StdClass();
        $object->create_at = time();
        if(file_exists($cache_file_path)){
            $data =  file_get_contents($cache_file_path);
            $object = json_decode($data);
        }
        $object->$key = $value;

        file_put_contents($cache_file_path, json_encode($object));
    }
    public static function reset(){
        $cache_file_path = plugin_dir_path(__DIR__.'data').'/.'.self::CACHE_KEY.'.cache';
        unlink($cache_file_path);
    }

}