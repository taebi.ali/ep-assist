<?php

/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/13/20
 * Time: 2:51 PM
 */
namespace EPAssist;
class EPAssetManager
{
    const ASSETS_PATH = '/assets';
    const CSS_PATH = '/css';
    const JS_PATH = '/js';
    const IMG_PATH = '/image';

    public $asset_files;
    public $plugin_directory;
    public $plugin_name;
    public $js_path;
    public $css_path;
    public $image_path;

    private $asset_version;

    function __construct($asset_directory)
    {

        $this->plugin_directory = $asset_directory;
        chdir($this->plugin_directory);
        $plugin_name = getcwd();
        $plugin_slug = 'plugins/';
        $name_pos = strpos($plugin_name, $plugin_slug);
        $this->plugin_name = substr($plugin_name, $name_pos+strlen($plugin_slug));



        $asset_directory = $this->plugin_directory.self::ASSETS_PATH;
        $this->js_path = '/wp-content/plugins/'.$this->plugin_name.self::ASSETS_PATH.self::JS_PATH;
        $this->css_path = '/wp-content/plugins/'.$this->plugin_name.self::ASSETS_PATH.self::CSS_PATH;
        $this->image_path = '/wp-content/plugins/'.$this->plugin_name.self::ASSETS_PATH.self::IMG_PATH;
        $asset_folders =  [
            $asset_directory.self::JS_PATH,
            $asset_directory.self::CSS_PATH
        ];
        $this->asset_files = [];
        foreach ($asset_folders as $asset_folder) {
            $files = scandir($asset_folder);
            foreach ($files as $file) {
                if(!in_array($file, ['.', '..'])){
                    $this->asset_files[]  = $asset_folder.'/'.$file;
                }
            }
        }
        //var_dump($this->plugin_directory, $this->plugin_name);
        //exit();
        //SavaLog('assets', $this->asset_files);
    }
    public  function get_asset_version(){
        if(!empty($this->asset_version)){
            return  $this->asset_version;
        }

        $file_times  = [];
        foreach ($this->asset_files as $asset_file) {
            $file_times[] = filemtime($asset_file);
        }



        $this->asset_version = md5(json_encode($file_times));

        //SavaLog('asset times' , [$file_times, $asset_version]);

        return $this->asset_version;
        //return '1.3.1';
    }
    public function addScript($name){
        wp_register_script(
            $name,
            $this->js_path.'/'.$name.'.js',
            false,
            $this->get_asset_version(),
            true
        );
        wp_enqueue_script( $name);
    }
    public function addStyle($name){
        wp_register_style(
            $name,
            $this->css_path.'/'.$name.'.css',
            false,
            $this->get_asset_version(),
            'all'
        );
        wp_enqueue_style( $name);
    }
}