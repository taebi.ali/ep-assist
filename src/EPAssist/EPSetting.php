<?php
namespace EPAssist;
class EPSetting
{
    const STORE_DATA_PATH = '/data/ep.dat';

    public static function store_setting($key, $value){
        $settings_data = file_get_contents(self::get_storing_path());
        if(!empty($settings_data))
            $settings = json_decode($settings_data, JSON_OBJECT_AS_ARRAY);
        else
            $settings = [];

        $settings[$key] = $value;
        file_put_contents(self::get_storing_path(), json_encode($settings));
    }
    public static function get_storing_path(){
        return dirname(__FILE__).self::STORE_DATA_PATH;
    }
    public static function get_setting($key){
        if(file_exists(self::get_storing_path())){
            $data =  file_get_contents(self::get_storing_path());
            $settings = json_decode($data, JSON_OBJECT_AS_ARRAY);
            if(isset($settings[$key])){
                return $settings[$key];
            }
        }
        return '';
    }
}