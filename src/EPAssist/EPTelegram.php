<?php
namespace EPAssist;
class EPTelegram
{

    const URI = 'https://albbservices.ir/api/send?token=';
    //const DEFAULT_CHAT = 'DbBK';
    //const DEBUG_CHAT = 'SalhaTest';
    //const PUBLIC_URI = 'https://www.salhacoffee.com/wp-content/plugins/salha-assist/assets/';
    //const BACKUP_DIRECTORY = '/../../assets/';

    const STORE_DATA_PATH = 'data/telegram_auth.dat';

    public function __construct($auth)
    {
        EPSetting::store_setting('telegram_auth', $auth);



    }
    public function set_chat($default, $debug){
        EPSetting::store_setting('telegram_default_chat', $default);
        EPSetting::store_setting('telegram_debug_chat', $debug);
    }
    public function set_backup_directory($backup_directory, $public_url){
        EPSetting::store_setting('telegram_backup_directory', $backup_directory);
        EPSetting::store_setting('telegram_public_uri', $public_url);
        EPJobs::Add('daily', [$this, 'ep_do_db_backup']);
        EPJobs::Add('weekly', [$this, 'ep_do_clean_backup']);
    }




    public static function get_auth(){
        return EPSetting::get_setting('telegram_auth');
    }
    public static function get_uri(){
        return self::URI.self::get_auth();
    }
    public static function get_backup_directory(){
        return EPSetting::get_setting('telegram_backup_directory');
    }
    public static function get_public_uri(){
        return EPSetting::get_setting('telegram_public_uri');
    }
    public static function get_default_chat(){
        return EPSetting::get_setting('telegram_default_chat');
    }
    public static function get_debug_chat(){
        return EPSetting::get_setting('telegram_debug_chat');
    }



    public function ep_do_db_backup(){
        EPTelegram::BackupDB();
    }
    public function ep_do_clean_backup(){
        PTelegram::CleanBackups();
    }




    public static function Send($chat, $message, $btn_text=null, $btn_url=null){
        return self::make_request($chat, $message, $btn_text, $btn_url);
    }
    public static function CacheSend($chat, $message, $ttl){
        global $ep_cache;
        if($ep_cache){
            $key = '_cache_send_'.$chat.'_'.md5($message);
            $data = $ep_cache->get($key);
            if($data){
                return null;
            }else{
                $result = self::Send($chat, $message);
                $ep_cache->set($key, time(), $ttl);
                return $result;
            }
        }
        return  null;
    }
    public static function BackupDB(){
        $db =  DB_NAME;
        $username = DB_USER;
        $password = DB_PASSWORD;
        $directory = dirname(__FILE__).self::get_backup_directory();
        $public_file_name = $db.'_'.date('Y_m_d_h_i_s');
        $file_name = $public_file_name.'_'.md5($public_file_name).'.sql';
        $file_path = $directory.$file_name;
        $file_path_tar = $directory.$file_name.'.tar.gz';
        $public_path = self::get_public_uri().$file_name;
        $public_path_tar = self::get_public_uri().$file_name.'.tar.gz';
        $host = DB_HOST;
        $command = 'mysqldump -h '.$host.' -u '.$username.' --password="'.$password.'" '.$db.' > '.$file_path;

        $output = shell_exec($command);
        #var_dump($output);

        $zip_command = 'tar -zcvf '.$file_path_tar.' '.$file_path;
        $output = shell_exec($zip_command);
        //self::Send(self::DEFAULT_CHAT, $public_path_tar);
        //self::Send(self::DEFAULT_CHAT, json_encode($output));

        $message = $public_file_name;
        self::make_request(self::get_default_chat(), $message, null, null, null, null, $public_path_tar);


        //unlink($file_path);
        //unlink($file_path_tar);

    }
    public static function CleanBackups(){
        $directory = dirname(__FILE__).self::get_backup_directory();
        $files = scandir($directory);
        $list = '';
        foreach ($files as $file) {
            if($file == '.' or $file == '..')
                continue;

            $sql = strpos($file, '.sql');
            if($sql>-1){
                unlink($directory.$file);
                $list .= $file.PHP_EOL;
            }
        }

        self::Send(self::get_default_chat(), 'remove old backups'.PHP_EOL.$list);
    }

    private static function make_request($chat, $message, $btn_text, $btn_url, $photo=null, $video=null, $file=null){
        $params  = [
            'message'=>[
                'channel'=>$chat,
                'auth'=>self::get_auth(),
                'data'=>[
                    'text'=>$message
                ]
            ]
        ];

        if(!empty($photo)){
            $params['message']['data']['photo'] = $photo;
        }elseif(!empty($video)){
            $params['message']['data']['video'] = $video;
        }elseif(!empty($file)){
            $params['message']['data']['file'] = $file;
        }elseif(!empty($btn_text) && !empty($btn_url)){
            $params['message']['data']['url'] = $btn_url;
            $params['message']['data']['url_text'] = $btn_text;
        }
        //var_dump($params);
        $params_json =  json_encode($params);
        $response = self::post($params_json);
        //var_dump($params_json, $response);

        return $response;
    }
    private static function post($params){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,self::get_uri());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $server_output = curl_exec($ch);

        curl_close ($ch);
        return $server_output;
    }
}



//add_action( 'sv_daily_jobs', 'shc_do_db_backup' );
//add_action( 'sv_weekly_jobs', 'shc_do_clean_backup' );

