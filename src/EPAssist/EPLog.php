<?php
namespace EPAssist;
class EPLog
{
    public function __construct($cat, $message)
    {
        error_log(json_encode([
            'category'=>$cat,
            'log'=>$message
        ]));
    }
}